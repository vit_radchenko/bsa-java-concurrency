package bsa.java.concurrency.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.nio.file.Files;

@Configuration
public class StarterHookConfiguration {

    @Autowired
    private ImageStorageProperties storageProperties;

    @Bean
    CommandLineRunner initPackages() {
        return args -> {
            Files.createDirectories(storageProperties.getBasePath());
        };
    }

}
