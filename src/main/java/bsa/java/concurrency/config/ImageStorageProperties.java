package bsa.java.concurrency.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.nio.file.Path;

@Component
public class ImageStorageProperties {

    @Autowired
    private Environment environment;

    private static final String BASE_PATH = System.getProperty("user.dir");

    public Path getBasePath() {
        return Path.of(BASE_PATH, environment.getProperty("storage.package.name"));
    }
}
