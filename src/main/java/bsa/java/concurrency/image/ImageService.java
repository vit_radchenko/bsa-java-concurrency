package bsa.java.concurrency.image;

import bsa.java.concurrency.image.dto.SearchResultDTO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

public interface ImageService {
    void save(MultipartFile[] files);

    List<SearchResultDTO> search(MultipartFile image, double threshold) throws ExecutionException, InterruptedException;

    void delete(UUID imageId);

    void purge();
}