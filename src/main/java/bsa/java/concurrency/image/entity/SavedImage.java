package bsa.java.concurrency.image.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@EqualsAndHashCode(of = "uuid")
@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class SavedImage {
    private UUID uuid;
    private Long hash;
    private String path;

    public static SavedImage of(UUID id, Long hash, String path) {
        return new SavedImage(id, hash, path);
    }
}
