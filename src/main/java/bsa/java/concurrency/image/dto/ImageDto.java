package bsa.java.concurrency.image.dto;

import bsa.java.concurrency.image.entity.SavedImage;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Data
@Getter
@Setter
public class ImageDto implements SearchResultDTO {
    @JsonIgnore
    private double matchPercent;
    @JsonIgnore
    private SavedImage savedImage;

    @Override
    @JsonProperty("id")
    public UUID getImageId() {
        return savedImage.getUuid();
    }

    @Override
    @JsonProperty("match")
    public Double getMatchPercent() {
        return matchPercent;
    }

    @Override
    @JsonProperty("image")
    public String getImageUrl() {
        return savedImage.getPath();
    }
}
