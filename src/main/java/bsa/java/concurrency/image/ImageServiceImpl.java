package bsa.java.concurrency.image;

import bsa.java.concurrency.fs.FileSystem;
import bsa.java.concurrency.hash.HashService;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.storage.InMemoryStorage;
import bsa.java.concurrency.storage.StorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Stream;

import static java.util.concurrent.CompletableFuture.supplyAsync;

@Service
public class ImageServiceImpl implements ImageService {
    private static final Logger logger = LoggerFactory.getLogger(ImageServiceImpl.class);
    private ExecutorService executors = Executors.newFixedThreadPool(10);

    private final FileSystem fileSystem;
    private final HashService hashService;
    private final StorageService inMemoryStorage;

    @Autowired
    public ImageServiceImpl(FileSystem fileSystem, HashService hashService, InMemoryStorage inMemoryStorage) {
        this.fileSystem = fileSystem;
        this.hashService = hashService;
        this.inMemoryStorage = inMemoryStorage;
    }

    public void save(MultipartFile[] files) {
        Stream.of(files)
                .map(this::convertToBufferedImage)
                .map(image -> supplyAsync(() -> image, executors))
                .map(future ->
                        future.thenAccept(bufferedImage -> {
                            fileSystem
                                    .saveImageToFileSystem(bufferedImage)
                                    .thenCombine(supplyAsync(() -> hashService.hash(bufferedImage), executors), (id, hash) -> {
                                        return inMemoryStorage.saveImage(hash, id);
                                    });
                        }).exceptionally(err -> {
                            logger.error("Exception happen: {}", err.getMessage());
                            return null;
                        })
                )
                .forEach(CompletableFuture::join);
    }

    public List<SearchResultDTO> search(MultipartFile image, double threshold) throws ExecutionException, InterruptedException {
        var bufferedImage = convertToBufferedImage(image);
        var hash = hashService.hash(bufferedImage);

        return supplyAsync(() -> inMemoryStorage.search(hash, threshold), executors)
                .thenCompose(list -> supplyAsync(() -> {
                    if (list.isEmpty()) {
                        fileSystem
                                .saveImageToFileSystem(bufferedImage)
                                .thenAccept(id -> {
                                    var toSaveHash = hashService.hash(bufferedImage);
                                    inMemoryStorage.saveImage(toSaveHash, id);
                                });
                    }
                    return list;
                })).exceptionally(err -> {
                    logger.error("Exception happen: {}", err.getMessage());
                    return null;
                }).get();
    }


    private BufferedImage convertToBufferedImage(MultipartFile file) {
        try {
            byte[] imageData = file.getBytes();
            ByteArrayInputStream bais = new ByteArrayInputStream(imageData);
            return ImageIO.read(bais);
        } catch (IOException e) {
            logger.error("Failed to convert file: {}, ex: {}", file.getName(), e.getMessage());
            throw new UncheckedIOException(e);
        }
    }

    public void delete(UUID imageId) {
        inMemoryStorage.delete(imageId).ifPresent(path -> {
            fileSystem.delete(path);
        });
    }

    public void purge() {
        inMemoryStorage.purge();
        fileSystem.clean();
    }
}