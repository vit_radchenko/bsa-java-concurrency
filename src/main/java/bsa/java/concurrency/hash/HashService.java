package bsa.java.concurrency.hash;

import org.springframework.stereotype.Service;

import java.awt.*;
import java.awt.image.BufferedImage;

@Service
public class HashService {

    public long hash(BufferedImage image) {
        long hash = 0;

        var result = image.getScaledInstance(9, 9, Image.SCALE_SMOOTH);
        var scaled = new BufferedImage(9, 9, BufferedImage.TYPE_BYTE_GRAY);
        scaled.getGraphics().drawImage(result, 0, 0, null);

        for (int i = 1; i < 9; i++) {
            for (int j = 1; j < 9; j++) {
                if (brightnessScore(scaled.getRGB(i, j)) > brightnessScore(scaled.getRGB(i - 1, j - 1))) {
                    hash |= 1;
                }
                hash = hash << 1;
            }
        }

        return hash;
    }

    private int brightnessScore(int rgb) {
        return rgb & 0b11111111;
    }
}