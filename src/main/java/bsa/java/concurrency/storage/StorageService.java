package bsa.java.concurrency.storage;

import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.image.entity.SavedImage;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface StorageService {
    void purge();

    Optional<String> delete(UUID imageId);

    List<SearchResultDTO> search(long hash, double threshold);

    SavedImage saveImage(long hash, UUID uuid);
}
