package bsa.java.concurrency.storage;

import bsa.java.concurrency.config.ImageStorageProperties;
import bsa.java.concurrency.image.dto.ImageDto;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.image.entity.SavedImage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Component
public class InMemoryStorage implements StorageService {
    private final ImageStorageProperties storageProperties;
    private Map<UUID, SavedImage> imagesMap = new ConcurrentHashMap<>();

    @Autowired
    public InMemoryStorage(ImageStorageProperties storageProperties) {
        this.storageProperties = storageProperties;
    }

    public void purge() {
        imagesMap.clear();
    }

    public Optional<String> delete(UUID imageId) {
        if (imagesMap.containsKey(imageId)) {
            return Optional.of(imagesMap.remove(imageId).getPath());
        }
        return Optional.empty();
    }

    public List<SearchResultDTO> search(long hash, double threshold) {
        return imagesMap.values()
                .stream()
                .map(image -> {
                    long savedHash = image.getHash();
                    double diffPercent = Long.bitCount(hash ^ savedHash) / 64d;
                    double matchPercent = 1 - diffPercent;
                    if (threshold < matchPercent) {
                        ImageDto dto = new ImageDto();
                        dto.setMatchPercent(matchPercent * 100);
                        dto.setSavedImage(image);
                        return dto;
                    }
                    return null;
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public SavedImage saveImage(long hash, UUID uuid) {
        return imagesMap.computeIfAbsent(uuid, id -> SavedImage.of(id, hash,
                Path.of(storageProperties.getBasePath().toString(), uuid.toString()).toString() + ".jpg"));
    }
}
