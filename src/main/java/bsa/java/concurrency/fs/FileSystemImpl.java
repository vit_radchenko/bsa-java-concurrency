package bsa.java.concurrency.fs;

import bsa.java.concurrency.config.ImageStorageProperties;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class FileSystemImpl implements FileSystem {
    private final ImageStorageProperties storageProperties;
    private ExecutorService executors = Executors.newFixedThreadPool(4);

    @Autowired
    public FileSystemImpl(ImageStorageProperties storageProperties) {
        this.storageProperties = storageProperties;
    }

    public CompletableFuture<UUID> saveImageToFileSystem(BufferedImage image) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                UUID id = UUID.randomUUID();
                String path = Path.of(storageProperties.getBasePath().toString(), String.format("%s.jpg", id.toString())).toString();
                ImageIO.write(image, "jpg", new File(path));
                return id;
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        }, executors);
    }

    @Override
    public void delete(String path) {
        try {
            Files.deleteIfExists(Path.of(path));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Override
    public void clean() {
        try {
            FileUtils.cleanDirectory(new File(storageProperties.getBasePath().toString()));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
